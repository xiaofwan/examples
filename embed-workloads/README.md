## Embed workload (AI sample applications) in a bootable container image

### Create a custom centos-bootc:stream9 image

This example assumes you will deploy on an x86_64 machine.
This example assumes you will build from an x84_64 system.
There is a [relevant issue](https://github.com/CentOS/centos-bootc/issues/282)
for cross-building with `podman build` and running nested podman commands.
The workload container images are pre-pulled into the bootable OCI image
(using nested `podman pull` within the Containerfiles).
This example was tested building from fedora.

Two example Containerfiles are included in this folder.

* [Containerfile-codegen](./Containerfile-codegen) - embeds an LLM-powered sample code generator chat application. Details on the application
can be found [here](https://github.com/containers/ai-lab-recipes/tree/main/code-generation). This Containerfile includes a model-server
that is meant to run with CPU - no additional GPU drivers or toolkits are embedded.

* [Containerfile-nvidia](./Containerfile-nvidia) - embeds an LLM-powered sample text summarizer application. Details on the application
can be found [here](https://github.com/containers/ai-lab-recipes/tree/main/summarizer). This Containerfile includes a model-server
meant to run with GPU acceleration. NVIDIA kernel drivers and the NVIDIA CUDA toolkit is embedded. This makes for a very large (> 10G) OCI image.

To build the derived bootc image for x86_64 architecture, run the following from the root of this repository:

```bash
# for CPU powered sample LLM application
podman build \
           --cap-add SYS_ADMIN \
           --platform linux/amd64 \
           -t quay.io/yourrepo/youros:tag \
           -f embed-workloads/Containerfile-codegen .

# for GPU powered sample LLM application
podman build \
           --cap-add SYS_ADMIN \
           --platform linux/amd64 \
           -t quay.io/yourrepo/youros:tag \
           -f embed-workloads/Containerfile-nvidia .

podman push quay.io/yourrepo/youros:tag
```

### Update a bootc-enabled system with the new derived image

To build a disk image from an OCI bootable image, you can refer to other examples in this repository.
For this example, we will assume a bootc enabled system is already running.
If already running a bootc-enabled OS, `bootc switch` can be used to update the system to target a new bootable OCI image with embedded workloads.

SSH into the bootc-enabled system and run:

```bash
bootc switch quay.io/yourrepo/youros:tag
```

The necessary image layers will be downloaded from the OCI registry, and the system will prompt you to reboot into the new operating system.
From this point, with any subsequent modifications and pushes to the `quay.io/yourrepo/youreos:tag` OCI image, your OS can be updated with:

```bash
bootc upgrade
```

### Accessing the embedded workloads

The text summarizer and code generator applications can be accessed by visiting port `8150` of the running bootc system.
They will be running as systemd services from podman quadlet files placed at `/etc/containers/systemd/` on the bootc system. For more information
about running containerized applications as systemd services with podman, refer to this
[podman quadlet post](https://www.redhat.com/sysadmin/quadlet-podman) or, [podman documentation](https://podman.io/docs)

To monitor the sample applications, SSH into the bootc system and run either:

```bash
systemctl status codegen 
or
systemctl status summarizer
```

You can also view the pods and containers that are managed with systemd by running:

```
podman pod list
podman ps -a
```

To stop the sample applications, SSH into the bootc system and run:

```bash
systemctl stop codegen 
or
systemctl stop summarizer
```

To run the sample application _not_ as a systemd service, stop the services then
run the appropriate commands based on the application you have embedded.

```bash
podman kube play /etc/containers/systemd/codegen.yaml
or
podman kube play /etc/containers/systemd/summarizer.yaml
```
